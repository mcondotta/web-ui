# Hacking

First of all, thank you for contributing to this project.  Here you will find
some tips about how to setup your development environment.

## Deploying in a local container for development

First, make sure you have the [API
Server](https://gitlab.com/insights4ci/api-server) running locally at
`http://localhost:8000`.

Now, build and run the docker container in your local machine:

```
$ docker build -t i4c-web-ui . -f Dockerfile.dev
$ docker run --net=host -v $(pwd):/app i4c-web-ui
```

When the build is finished you will receive a message saying the app is up and
running:

```
  App running at:
      - Local:   http://localhost:8080/ 
      - Network: http://192.168.69.117:8080/
```

## Reporting issues

If you found any issue during the bootstrap, please feel free to [Open a new
issue](https://gitlab.com/insights4ci/web-ui/-/issues).

## Before submitting a MR

Make sure you have read our Contributor Guide at [CONTRIBUTING.md](CONTRIBUTING.md), before
sending a MR.
